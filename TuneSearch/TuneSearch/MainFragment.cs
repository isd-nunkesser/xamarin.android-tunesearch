﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Arch.Lifecycle;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using BasicCleanArch;

namespace TuneSearch
{
    public class MainFragment : Android.Support.V4.App.Fragment, IDisplayer<IEnumerable<ItemViewModel>>
    {
        private IOnFragmentInteractionListener Listener;
        private Button searchButton;
        private TrackListViewModel viewModel;
        private IUseCase<SearchRequest, IEnumerable<ItemViewModel>> _interactor = new SearchInteractor();

        public override void OnAttach(Context context)
        {
            base.OnAttach(context);
            if (context is IOnFragmentInteractionListener)
            {
                Listener = (TuneSearch.MainFragment.IOnFragmentInteractionListener)context;
            }
            else
            {
                throw new AndroidRuntimeException($"{context.ToString()} must implement IOnFragmentInteractionListener");
            }
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
             return inflater.Inflate(Resource.Layout.fragment_main, container, false);

            
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            searchButton = view.FindViewById<Button>(Resource.Id.searchButton);
            searchButton.Click += (s, e) =>
            {
                searchButton.Enabled = false;
                var searchTermEntry = view.FindViewById<EditText>(Resource.Id.searchTermEditText);
                var term = searchTermEntry.Text;
                _interactor.Execute(new SearchRequest { term = term }, this);
            };
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            viewModel = ViewModelProviders.Of(Activity).Get(Java.Lang.Class.FromType(typeof(TrackListViewModel))) as TrackListViewModel;
        }

        public void Display(Result<IEnumerable<ItemViewModel>> response)
        {
            searchButton.Enabled = true;
            response.Match(
             success =>
             {
                 viewModel.submitData(success);
                 Listener.NavigateToNextFragment();
             },
             failure => Log.Error("MainFragment", failure.ToString())
             );
        }

        public interface IOnFragmentInteractionListener
        {
            void NavigateToNextFragment();
        }

    }
}
