﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using static TuneSearch.MainFragment;

namespace TuneSearch
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, IOnFragmentInteractionListener
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            if (savedInstanceState==null)
            {
                var transaction = SupportFragmentManager.BeginTransaction();
                var fragment = new MainFragment();
                transaction.Replace(Resource.Id.fragment, fragment).Commit();
            }
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public void NavigateToNextFragment()
        {
            var transaction = SupportFragmentManager.BeginTransaction();
            var fragment = new TrackListFragment();
            transaction.Replace(Resource.Id.fragment, fragment).AddToBackStack(null).Commit();
        }
    }
}