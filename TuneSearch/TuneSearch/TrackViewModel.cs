﻿using System;
using System.Collections.ObjectModel;

namespace TuneSearch
{
    public class TrackViewModel : ItemViewModel
    {
        public string ArtistName { get; set; }
        public string Image { get; set; }        
    }
}
