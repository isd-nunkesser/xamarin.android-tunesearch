﻿using System.Collections;
using System.Collections.Generic;
using Android.Arch.Lifecycle;
using Android.Runtime;

namespace TuneSearch
{
    public class TrackListViewModel : ViewModel
    {

        private MutableLiveData data = new MutableLiveData();
        public LiveData Data
        {
            get => data;
        }

        public TrackListViewModel()
        {
        }

        public void submitData(IEnumerable<ItemViewModel> items)
        {
            data.PostValue(new JavaList(items));
        } 
    }
}