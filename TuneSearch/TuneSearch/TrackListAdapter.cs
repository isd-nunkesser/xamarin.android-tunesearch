﻿using System;
using Android.Content;
using Android.Support.V7.RecyclerView.Extensions;
using Android.Support.V7.Util;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Square.Picasso;

namespace TuneSearch
{
    public class TrackListAdapter : ListAdapter
    {
        private readonly Context context;

        public TrackListAdapter(Context context) : base(new ItemCallback())
        {
            this.context = context;
        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            if (viewType==typeof(ItemViewModel).GetHashCode())
            {
                return new ItemViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.list_item_header, parent, false));
            } else
            {
                return new TrackViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.fragment_track, parent, false));
            }
           
           
        }

        public override int GetItemViewType(int position)
        {
            return GetItem(position).GetType().GetHashCode();
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = GetItem(position);
            ((ItemViewHolder)holder).ContentView.Text = ((ItemViewModel)item).Content;
            if (holder is TrackViewHolder && item is TrackViewModel)
            {
                ((TrackViewHolder)holder).ArtistTextView.Text = ((TrackViewModel)item).ArtistName;
                Picasso.With(context)
                    .Load(((TrackViewModel)item).Image)
                    .Resize(200, 200)
                    .CenterCrop()
                    .Into(((TrackViewHolder)holder).ArtworkImageView);
            }
        }

        class ItemViewHolder : RecyclerView.ViewHolder
        {
            public TextView ContentView { get; set; }
            public ItemViewHolder(View view) : base(view)
            {
                ContentView = view.FindViewById<TextView>(Resource.Id.content);
            }
        }

        class TrackViewHolder : ItemViewHolder
        {
            public TextView ArtistTextView { get; set; }
            public ImageView ArtworkImageView { get; set; }
            public TrackViewHolder(View view) : base(view)
            {
                ArtistTextView = view.FindViewById<TextView>(Resource.Id.artist);
                ArtworkImageView = view.FindViewById<ImageView>(Resource.Id.artwork);
            }
        }

        class ItemCallback : DiffUtil.ItemCallback
        {
            public override bool AreContentsTheSame(Java.Lang.Object p0, Java.Lang.Object p1)
            {
                return p0.Equals(p1);
            }

            public override bool AreItemsTheSame(Java.Lang.Object p0, Java.Lang.Object p1)
            {
                return p0 == p1;
            }
        }
    }
}
