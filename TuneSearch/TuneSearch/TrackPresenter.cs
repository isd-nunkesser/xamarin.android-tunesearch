﻿using System;
using BasicCleanArch;

namespace TuneSearch
{
    public class TrackPresenter : IPresenter<TrackEntity, TrackViewModel>
    {
        public TrackViewModel present(TrackEntity entity)
        {
            return
                new TrackViewModel
                {
                    Image = entity.artworkUrl100,
                    Content = $"{entity.trackNumber}  - {entity.trackName}",
                    ArtistName = entity.artistName
                };
        }
    }
}
