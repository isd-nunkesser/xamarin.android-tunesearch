﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Arch.Lifecycle;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace TuneSearch
{
    public class TrackListFragment : Android.Support.V4.App.Fragment
    {
        private TrackListAdapter adapter;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            adapter = new TrackListAdapter(Activity);
            var viewModel = ViewModelProviders.Of(Activity).Get(Java.Lang.Class.FromType(typeof(TrackListViewModel))) as TrackListViewModel;
            adapter.SubmitList((System.Collections.IList)viewModel.Data.Value);
            var recyclerView = Activity.FindViewById<RecyclerView>(Resource.Id.list);
            recyclerView.SetLayoutManager(new LinearLayoutManager(Activity));
            recyclerView.SetAdapter(adapter);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.fragment_track_list, container, false);            
        }
    }
}
