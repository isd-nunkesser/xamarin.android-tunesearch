﻿using System.Collections.Generic;
using System.Linq;
using BasicCleanArch;

namespace TuneSearch
{
    public class SearchInteractor : IUseCase<SearchRequest, IEnumerable<ItemViewModel>>
    {
        public async void Execute(SearchRequest request, IDisplayer<IEnumerable<ItemViewModel>> outputBoundary)
        {
            var gatewayResponse = await new ITunesSearchGateway().GetSongs(request.term);
            gatewayResponse.Match(success =>
            {
                var collections = success.OrderBy(t => t).GroupBy(t => t.collectionName);
                var trackList = new List<ItemViewModel>();
                foreach (var result in collections)
                {
                    trackList.Add(new ItemViewModel() { Content = result.Key });
                    foreach (var track in result)
                    {
                        trackList.Add(new TrackPresenter().present(track));
                    }                    
                }
                 

                outputBoundary.Display(new Result<IEnumerable<ItemViewModel>>(trackList));
            }, failure =>
            {
                outputBoundary.Display(new Result<IEnumerable<ItemViewModel>>(failure));
            });
        }
    }
}